// Custom JS 

/*
* @author: Faisal Saeed
*/

/*app carousel*/
$(document).ready(function () {

  /*growing with us*/
  var swiper = new Swiper(".growing-with-us", {
    slidesPerView: 1,
    spaceBetween: 10,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 5,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    },
  });

  /*scroll to top*/
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });
  $('.scrollup').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 600);
    return false;
  });

  /*sections animation*/
  (function ($) {
    'use strict';
    // variables
    var $isAnimatedSecond = $('.second .is-animated'),
      $isAnimatedSecondSingle = $('.second .is-animated__single'),
      $isAnimatedThird = $('.third .is-animated'),
      $isAnimatedThirdSingle = $('.third .is-animated__single'),
      $isAnimatedFourth = $('.fourth .is-animated'),
      $isAnimatedFourthSingle = $('.fourth .is-animated__single'),
      $isAnimatedFifth = $('.fifth .is-animated'),
      $isAnimatedFifthSingle = $('.fifth .is-animated__single'),
      $isAnimatedSixth = $('.sixth .is-animated'),
      $isAnimatedSixthSingle = $('.sixth .is-animated__single'),
      $isAnimatedSeventh = $('.seventh .is-animated'),
      $isAnimatedSeventhSingle = $('.seventh .is-animated__single'),
      $isAnimatedEight = $('.eight .is-animated'),
      $isAnimatedEightSingle = $('.eight .is-animated__single'),
      $isAnimatedNinth = $('.ninth .is-animated'),
      $isAnimatedNinthSingle = $('.ninth .is-animated__single'),
      $isAnimatedTenth = $('.tenth .is-animated'),
      $isAnimatedTenthSingle = $('.tenth .is-animated__single');
    // initialize fullPage
    $('#fullpage').fullpage({
      anchors: ['clueYou', 'worldClassExperts', 'howItWorks', 'categories', 'verifiedAnswers', 'discover', 'expert', 'faqs', 'whyClue', 'joinUs'],
      navigation: true,
      navigationPosition: 'right',
      scrollOverflow: true,
      navigationTooltips: ['Clueyou', 'World Class Experts', 'How It Works?', 'Categories', 'Verified Answers', 'Discover Career Prospect', 'Become An Expert', 'Faqs', 'Why Clue', 'Join Us'],
      showActiveTooltip: false,
      onLeave: function (index, nextIndex, direction) {
        if (index == 1 && nextIndex == 2) {
          $isAnimatedSecond.addClass('animated fadeInUp');
          $isAnimatedSecond.eq(0).css('animation-delay', '.3s');
          $isAnimatedSecond.eq(1).css('animation-delay', '.6s');
          $isAnimatedSecond.eq(2).css('animation-delay', '.9s');
          $isAnimatedSecondSingle.addClass('animated fadeInUp').css('animation-delay', '1s');
        }

        else if ((index == 1 || index == 2) && nextIndex == 3) {
          $isAnimatedThird.eq(0).addClass('animated zoomIn').css('animation-delay', '.3s');
          $isAnimatedThirdSingle.addClass('animated zoomIn').css('animation-delay', '.6s');
        }
        else if ((index == 1 || index == 2 || index == 3) && nextIndex == 4) {
          $isAnimatedFourth.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedFourthSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4) && nextIndex == 5) {
          $isAnimatedFifth.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedFifthSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5) && nextIndex == 6) {
          $isAnimatedSixth.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedSixthSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6) && nextIndex == 7) {
          $isAnimatedSeventh.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedSeventhSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7) && nextIndex == 8) {
          $isAnimatedEight.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedEightSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7 || index == 8) && nextIndex == 9) {
          $isAnimatedNinth.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedNinthSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
        else if ((index == 1 || index == 2 || index == 3 || index == 4 || index == 5 || index == 6 || index == 7 || index == 8 || index == 9) && nextIndex == 10) {
          $isAnimatedTenth.eq(0).addClass('animated fadeInUp').css('animation-delay', '0.3s');
          $isAnimatedTenthSingle.addClass('animated fadeInUp').css('animation-delay', '0.6s');
        }
      }
    });
  })(jQuery);

});



